#include<stdio.h>
#include <stdlib.h>
void mem_copy(void *dest, const void *src, unsigned int n);
int main()
{
	char* s = (char*)malloc(20*sizeof(char));
	char* dest = (char*)malloc(20*sizeof(char));
	int bits;
	printf("Enter test String (max length 20) : ");
	scanf("%s",s);
	printf("How many bits you wants to copy : ");
	scanf("%d",&bits);
	mem_copy(dest,s,bits);
	printf("\nSource :");
	for(int i=0;i<=bits;i++)
	{
		printf("\n%d %c ",s+i,*(s+i));
	}
	printf("\n\nDestination :");
	for(int i=0;i<=bits;i++)		
	{
		printf("\n%d %c ",dest+i,*(dest+i));
	}
}
void mem_copy(void *dest_p, const void *src_p, unsigned int n)
{
	char* dest = (char*)dest_p;
	char* src = (char*)src_p;
	int counter = 0;
	for(counter;counter<=(int)n;counter++)
	{
		dest[counter] = src[counter];		
	}
}

/*
Answer :

Source :
7214816 q
7214817 w
7214818 e
7214819 r
7214820 t

Destination :
7214848 q
7214849 w
7214850 e
7214851 r
7214852 t
*/