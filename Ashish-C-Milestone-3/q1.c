//1.Create a program to input a set of integers and sort them in ascending or descending order depending on users choice
#include<stdio.h>
int main()
{
	int arry[10],ary_len,i,j,temp;
	char ch;
	printf("How many numbers you want's to enter (Max 10): ");
	scanf("%d",&ary_len);
	printf("Enter you %d numbers : ",ary_len);
	for(i=0;i<ary_len;i++)
	{
		scanf("%d",&arry[i]);
	}
	printf("Entered array : ");
	for(i=0;i<ary_len;i++)
	{
		printf(" %d",arry[i]);
	}
	while(ch!='q')
	{
	printf("\nSelect assending or dissending (a/d)\n q for quit : ");
	scanf(" %c",&ch);
	
	if(ch=='a')
	{
		printf("**** Assending Order ****\n");
		for(i=0;i<ary_len;i++)
		{
			for(j=i;j<ary_len;j++)
			{
				if(arry[i]>arry[j])
				{
				temp = arry[i];
				arry[i] = arry[j];
				arry[j] = temp;				
				}
			}
		}
		for(i=0;i<ary_len;i++)
		{
		printf(" %d",arry[i]);
		}
	}
	else if(ch=='d')
	{
		printf("**** Dissending Order ****\n");
		for(i=0;i<ary_len;i++)
		{
			for(j=i;j<ary_len;j++)
			{
				if(arry[i]<arry[j])
				{
				temp = arry[i];
				arry[i] = arry[j];
				arry[j] = temp;				
				}
			}
		}
		for(i=0;i<ary_len;i++)
		{
		printf(" %d",arry[i]);
		}
	}
	else if(ch=='q')
	{break;}
	else
	{
		printf(" Please select a or d !!!!");
	}
	}
}

/*
Answer :

Enter you 5 numbers : 5 7 3 1 9
Entered array :  5 7 3 1 9
Select assending or dissending (a/d)
 q for quit : a
**** Assending Order ****
 1 3 5 7 9
Select assending or dissending (a/d)
 q for quit : d
**** Dissending Order ****
 9 7 5 3 1
Select assending or dissending (a/d)
 q for quit : q
 
 */