//2.Create a program to input a set of integers and then search the array to see if a particular integer exists in the array
#include<stdio.h>
int main()
{
	int arry[10],ary_len,n,temp=0;
	printf("How many numbers you want's to enter (Max 10): ");
	scanf("%d",&ary_len);
	printf("Enter you %d numbers : ",ary_len);
	for(int i=0;i<ary_len;i++)
	{
		scanf("%d",&arry[i]);
	}
	printf("Entered array : ");
	for(int i=0;i<ary_len;i++)
	{
		printf(" %d",arry[i]);
	}
	
	printf("\nEnter an number for search : ");
	scanf("%d",&n);
	for(int i=0;i<ary_len;i++)
	{
		if(arry[i]==n)
		{
			printf("%d is found at %d index",n,i);
			temp = 1;
			break;
		}
		
	}
	if(temp==0)
	{
		printf("%d is not found",n);
	}
	return 0;
}
/*
Answer :

How many numbers you want's to enter (Max 10): 5
Enter you 5 numbers : 7 8 2 7 1
Entered array :  7 8 2 7 1
Enter an number for search : 2
2 is found at 2 index
*/