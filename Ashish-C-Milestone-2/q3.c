//3.int is_prime(unsigned int x) - Returns 1 if x is a prime number and returns 0 if it is not
#include<stdio.h>
int is_prime(int n);
int main()
{
	unsigned int x;
	printf("Enter x : ");
	scanf("%d",&x);
	if(is_prime(x))
	{
		printf("prime number");
	}
	else{
		printf("not prime number");
	}
	return 0;
}

int is_prime(int n)
{
	int ans, i=2,flag;
	while(i<n)
	{
		if(n%i==0)
		{
			flag = 0;
			break;
		}
		else
		{
			flag = 1;
		}
		i++;
	}
	
	return flag;
}
/*
Answer :

Enter x : 21
not prime number
*/