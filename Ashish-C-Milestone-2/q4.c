//4.double FV(double rate, unsigned int nperiods, double PV) - Calculates and returns the Future Value of an investment based on the compound interest formula FV = PV * (1+rate)nperiods
#include<stdio.h>
double FV(double rate, unsigned int nperiods, double PV);
int main()
{
	double rate,pv,fv;
	unsigned int np;
	
	printf("Enter rate : ");
	scanf("%lf",&rate);
	printf("Enter nperiods : ");
	scanf("%d",&np);
	printf("Enter pv : ");
	scanf("%2lf",&pv);
	
	fv = FV(rate,np,pv);
	printf("Furure Value : %2lf",fv);
	
	return 0;
}

double FV(double rate, unsigned int nperiods, double pv)
{
	double fv;

	fv = pv * (1+ rate)*nperiods;
	
	return fv;
}

/*
Answer :

Enter rate : 2
Enter nperiods : 2
Enter pv : 2
Furure Value : 12.000000
*/