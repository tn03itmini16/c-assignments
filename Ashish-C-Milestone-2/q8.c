//8.A function to check if a number is odd or even
#include<stdio.h>
int odd_even(int n);
int main()
{
	int num;
	printf("Enter Number : ");
	scanf("%d",&num);
	if(odd_even(num)==0)
	{
		printf("%d is Even",num);
	}
	else
	{
		printf("%d is Odd",num);
	}
}

int odd_even(int n)
{
	if(n%2==0)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

/*
Answer :

Enter Number : 22
22 is Even
*/