//12.nPr - Permutations = P(n,r)=n! (n−r)!
#include<stdio.h>
 int fact(int n);
int main()
{
	int n,r,ans;
	printf("Enter n and r value for nPr : ");
	scanf("%d %d",&n,&r);
	ans = fact(n)*fact(n-r);
	printf("nPr = %d",ans);
}

 int fact(int n)
{
	int ans=1;
	while(n>=1)
	{
		ans = ans * n;
		n--;
	}
	return ans;
}

/*
Answer :

Enter n and r value for nPr : 3 4
nPr = 6
*/