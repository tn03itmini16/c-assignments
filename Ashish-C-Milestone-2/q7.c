//7.Function for finding and printing all prime factors of a number
#include<stdio.h>
int is_prime(int n);
void factor(int n);

int main()
{
	int num;
	printf("Enter a number : ");
	scanf("%d",&num);
	printf("Prime factors of %d is : ",num);
	factor(num);
}

void factor(int n)
{
	int i = n;
	while(i>0)
	{
		if(n%i==0)
		{
			if(is_prime(i))
			{printf(" %d",i);}
		}
		i--;
	}
}

int is_prime(int n)
{
	int ans, i=2,flag;
	while(i<n)
	{
		if(n%i==0)
		{
			flag = 0;
			break;
		}
		else
		{
			flag = 1;
		}
		i++;
	}
	
	return flag;
}

/*
Answer :

Enter a number : 24
Prime factors of 24 is :  3 2 1
*/