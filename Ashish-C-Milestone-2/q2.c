//2.int gcd(int a, int b) - Calculates and returns the Greatest Common divisor of a and b
#include<stdio.h>
int gcd(int a, int b);
int main()
{
int a,b,ans;
printf("Enter a : ");
scanf("%d",&a);
printf("Enter b :");
scanf("%d",&b);
 
 ans = gcd(a,b); 
 printf("Answer is = %d",ans);
 
 return 0;
}

int gcd(int a,int b)
{
	int i;
	if(a ==0 || b == 0)
	{
		return 0;
	}
	else
	{
	if(a<b)
		{
			i=a-1;
		}
		else
		{
			i=b-1;
		}
			
		while(i>0)
		{
			if(a%i==0 && b%i==0)
			{
				return i;
				break;
			}
			i--;
				
		}
	}
}

/*
Answer :

Enter a : 24
Enter b :56
Answer is = 8
*/