//9.A function to get LCM of 3 numbers
#include<stdio.h>
int LCM(int n1,int n2,int n3);
int greatest(int n1,int n2,int n3);

int main()
{
	int n1,n2,n3;
	printf("Enter 3 Numbers :");
	scanf("%d %d %d",&n1,&n2,&n3);
	printf("LCM = %d",LCM(n1,n2,n3));
}
int LCM(int n1,int n2,int n3)
{
	int ans=1,j;
	
		j = greatest(n1,n2,n3)+1;
		
		while(1)
		{
			if(j%n1==0 && j%n2 == 0 && j%n3 == 0)
			{
				ans = j;
				break;
			}
			j++;
		}

	return ans;
}
int greatest(int n1,int n2,int n3)
{
	if(n1>n2 && n1>n3){return n1;}
	else if(n2>n1 && n2>n3){return n2;}
	else if(n3>n1 && n3>n2){return n3;}
}
/*
Answer :

Enter 3 Numbers :20 30 50
LCM = 300
*/