//6.Function for finding and printing all factors of a number
#include<stdio.h>
void factor(int n);
int main()
{
	int num;
	printf("Enter Number : ");
	scanf("%d",&num);
	printf("factors of %d is :",num);
	factor(num);
}

void factor(int n)
{
	int i = n;
	while(i>0)
	{
		if(n%i==0)
		{
			printf(" %d",i);
		}
		i--;
	}
}

/*
Answer :

Enter Number : 16
factors of 16 is : 16 8 4 2 1
*/