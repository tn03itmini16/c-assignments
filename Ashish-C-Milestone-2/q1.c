//1.double pow(double x, int n) - Calculates and returns xn
#include<stdio.h>
#include<math.h>
int main()
{
double x,n;
double ans;
printf("Enter x : ");
scanf("%lf",&x);
printf("Enter Power :");
scanf("%lf",&n);
 
 ans = pow(x,n); 
 printf("Answer is = %2lf",ans);
 
 return 0;
}

/*
Answer :

Enter x : 2.1
Enter Power :3
Answer is = 9.261000
*/