//3.A program to convert temperature in Farenheit(F) to Celcius(C)
#include<stdio.h>
int main()
{
	float fa,ce;
	printf("Enter Farenheit value = ");
	scanf("%f",&fa);
	ce = (fa-32)/1.8;
	printf("Celcius = %f",ce);
	
	return 0;
}

/*
Answer :

Enter Farenheit value = 234
Celcius = 112.222221
*/