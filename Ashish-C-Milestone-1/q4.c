//4.A program to calculate the Area of a Circle
#include<stdio.h>
int main()
{
	float area,r;
	printf("Enter Radius : ");
	scanf("%f",&r);
	area = 3.14*r*r;
	printf("Area of circle = %f",area);
	
	return 0;
}

/*
Answer :

Enter Radius : 2
Area of circle = 12.560000
*/