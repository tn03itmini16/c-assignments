//1.A program to convert weight entered in pounds(lbs) to kilogram(kg)
#include<stdio.h>
int main()
{
 float pound,kg;
 printf("Enter Pound Value : ");
 scanf("%f",&pound);
 kg = pound*0.453592;
 printf("Kilogram = ");
 printf("%f",kg);
 
}

/*
Answer :

Enter Pound Value : 1234
Kilogram = 559.732544
*/