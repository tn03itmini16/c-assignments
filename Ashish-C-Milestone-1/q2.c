//2.A program to convert length entered in inches to centimeters(cm)
#include<stdio.h>
int main()
{
	float in,cen;
	printf("Enter Inches = ");
	scanf("%f",&in);
	cen = in*2.54;
	printf("Centimeters = %f",cen);
	
	return 0;
}

/*
Answer :

Enter Inches = 1.2
Centimeters = 3.048000
*/