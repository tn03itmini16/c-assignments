#include<stdio.h>
#include<stdlib.h>
void sort(void *a,int len,int siz)
{	
	if(siz==4)
	{
		
		int *ary = (int*)a;
		int temp;
		for(int i=0;i<len;i++)
		{
		for(int j=i;j<len;j++)
		{
			if(ary[i]>ary[j])
			{
				temp = ary[i];
				ary[i] = ary[j];
				ary[j] = temp;
			}
		}
		}
	}
	else if(siz==1)
	{
		char temp;
		for(int i=0;i<len;i++)
		{
			for(int j=i;j<len;j++)
			{
				if(ary[i]>ary[j])
				{
					temp = ary[i];
					ary[i] = ary[j];
					ary[j] = temp;
				}
			}
		}
	}
	else if(siz==8)
	{
	double *ary = (double*)a;
	double temp;
	for(int i=0;i<len;i++)
	{
		for(int j=i;j<len;j++)
		{
			if(ary[i]>ary[j])
			{
				temp = ary[i];
				ary[i] = ary[j];
				ary[j] = temp;
			}
		}
	}
}
}
int main()
{
	int ch,len;
	int int_ary[10];
	double double_ary[10];
	char char_ary[10];
	
	printf("\n1. int\n2. double\n3. char\nSelect Data type :");
	scanf("%d",&ch);
	switch(ch)
	{
	 case 1 :
				 printf("Enter array length(max 10) : ");
				 scanf("%d",&len);
				 printf("\nEnter array : ");
				 for(int i=0;i<len;i++)
				 {
					scanf("%d",&int_ary[i]);
				 }
				 sort(int_ary,len,sizeof(int));
				 printf("\nSorted arry : ");
				 for(int i=0;i<len;i++)
				 {
					 printf(" %d",int_ary[i]);
				 }
				 break;
	 case 2 :
				 printf("Enter array length(max 10) : ");
				 scanf("%d",&len);
+				 printf("\nEnter array : ");
				  for(int i=0;i<len;i++)
				 {
					scanf("%lf",&double_ary[i]);
				 }
				 sort(double_ary,len,sizeof(double));
				 printf("\nSorted arry : ");
				 for(int i=0;i<len;i++)
				 {
					 printf(" %lf",double_ary[i]);
				 }
				 break;
	 case 3 :
				 printf("Enter array length(max 10) :  ");
				 scanf("%d",&len);
				 printf("\nEnter array : ");
				 for(int i=0;i<len;i++)
				 {
					scanf(" %c",&char_ary[i]);
				 }
				 sort(char_ary,len,sizeof(char));
				 printf("\nSorted arry : ");
				 for(int i=0;i<len;i++)
				 {
					 printf(" %c",char_ary[i]);
				 }
				 break;
				
	 default : printf("Enter currect choice !!");
				break;
	}
}

/*
Answer :

1. int
2. double
3. char
Select Data type :1
Enter array length(max 10) : 5

Enter array : 9 6 3 7 1

Sorted arry :  1 3 6 7 9

Milestone-8>a

1. int
2. double
3. char
Select Data type :2
Enter array length(max 10) : 5

Enter array : 3.1 2.8 4.5 1.0 9.9

Sorted arry :  1.000000 2.800000 3.100000 4.500000 9.900000

Milestone-8>a

1. int
2. double
3. char
Select Data type :3
Enter array length(max 10) :  5

Enter array : z y r a g

Sorted arry :  a g r y z
*/