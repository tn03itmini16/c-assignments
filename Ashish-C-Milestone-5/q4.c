//4. int str_find_char(char *str, char *ch) - returns the position where ch is in str and if not present returns -1
#include<stdio.h>
int str_find_char(char *str, char ch);
int main()
{
	char s[21];
	char c;
	printf("Enter String (max length 20) : ");
	scanf("%s",&s);
	printf("%s",s);
	printf("Enter charactor : ");
	scanf(" %c",&c);
	printf("Location : %d",str_find_char(s,c));
}
int str_find_char(char *str, char ch)
{
	int i = 0,flag;
	while(str[i]!='\0')
	{
		if(str[i]==ch)
		{
			flag = 1;
			break;
		}
		i++;
	}
	if(flag == 1)
	{return i;}
	else 
	{return -1;}
}
/*
Answer :

Enter String (max length 20) : programming
programmingEnter charactor : i
Location : 8

*******************************************

Enter String (max length 20) : qwerty
qwertyEnter charactor : x
Location : -1
*/