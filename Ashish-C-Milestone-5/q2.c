//2. int str_copy(char *str1, char *str2) - copies string 2 into string 1, returns 0 on success or -1 on error
#include<stdio.h>
int str_copy(char *str1, char *str2);
int main()
{
	int result;
	char s1[21],s2[21];
	printf("Enter String (max length 20) : ");
	scanf("%s",&s1);
	result = str_copy(s1,s2);
	printf("str1 = %s | str2 = %s | status = %d",s1,s2,result);
}

int str_copy(char *str1, char *str2)
{
	char *st1,*st2;
	st1 = str1;
	st2 = str2;
	int flag=0;
	while(*str1 !='\0')
	{
		*str2 = *str1;
		str1++;
		str2++;
	}
	*str2 = '\0';
	while(*st1 !='\0' && *st2 !='\0')
	{
		if(*st1 != *st2)
		{
			flag = -1;
			break;
		}
		st1++;
		st2++;
	}
	return flag;
}

/*
Answer :

Enter String (max length 20) : qwerty
str1 = qwerty | str2 = qwerty | status = 0
*/