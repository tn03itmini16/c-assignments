//5. int str_find_substring(char *str1, char *str2) - returns the position where str2 is in str1 and if not present returns -1.
#include<stdio.h>
int str_find_substring(char *str1, char *str2);
int main()
{
	char s1[21],s2[20];
	printf("Enter 1st String (max 20) : ");
	scanf("%s",&s1);
	printf("Enter 2nd String (max 20) : ");
	scanf("%s",&s2);
	printf("Result is : %d",str_find_substring(s1,s2));
}
int str_find_substring(char *str1, char *str2)
{
	int i=0,j=0,str2_len=0,count=0,start=0;
	while(str2[j]!='\0')
	{
		str2_len++;
		j++;
	}
	j=0;
	while(str1[i]!='\0' || str2[j]!='\0')
	{
		if(str1[i]==str2[j])
		{
			if(start==0){start=i;}
			count++;
			i++;
			j++;
		}
		else
		{
			i++;
			if(str2[j]!='\0')
			{
			j=0;
			start=0;
			count=0;
			}
			
		}

	}
	if(count==str2_len){
		return start;
	}
	else
	{return -1;}
}
/*
Answer :

Enter 1st String (max 20) : programming
Enter 2nd String (max 20) : gram
Result is : 3
*/