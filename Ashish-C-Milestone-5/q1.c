//1. int str_length(char *str) - returns the length of the string or -1 on error

#include<stdio.h>
int str_length(char *str);
int main()
{
	char s[21];
	printf("Enter String (max length 20) : ");
	scanf("%s",&s);
	printf("String : %s | Length : %d",s,str_length(s));
	return 0;
}

int str_length(char *str)
{
	int count=0;
	while(*str != '\0')
	{
		count++;
		str++;
	}
return count;
}

/*
Answer :

Enter String (max length 20) : programming
String : programming | Length : 11
*/
