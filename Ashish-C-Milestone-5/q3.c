//3. int str_compare(char *str1, char *str2) - if str1 is less than str2 the function should return -1 if str1 is greater than str2 it should return 1 and if both are equal then return 0
//When you think of strings and comparing them you should compare them based on alphabetical ordering or character ASCII value rather than length.

#include<stdio.h>
int str_compare(char *str1, char *str2);
int main()
{
	char s1[21],s2[20];
	int result;
	printf("Enter 1st String (max 20) : ");
	scanf("%s",&s1);
	printf("Enter 2nd String (max 20) : ");
	scanf("%s",&s2);
	result = str_compare(s1,s2);
		printf("Result : %d",result);
}
int str_compare(char *str1, char *str2)
{
	int sum1=0, sum2=0;
	while(*str1 != '\0')
	{
		sum1= sum1+*str1;
		str1++;
	}
	while(*str2 != '\0')
	{
		sum2=sum2+*str2;
		str2++;
	}
	if(sum1==sum2)
	{return 0;}
	else if(sum1<sum2)
	{return -1;}
	else
	{return 1;}
}
/*
Answer :

Enter 1st String (max 20) : programming
Enter 2nd String (max 20) : nrogramming
Result : 1

****************************************

Enter 1st String (max 20) : programming
Enter 2nd String (max 20) : programming
Result : 0

****************************************

Enter 1st String (max 20) : programming
Enter 2nd String (max 20) : rrogramming
Result : -1
*/